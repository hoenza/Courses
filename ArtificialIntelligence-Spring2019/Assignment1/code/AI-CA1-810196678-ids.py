import time
import copy

# ------------------------------------------------------------------

trueBoards = []

def fill(board, size) :
    if (size == 8) :
        trueBoards.append(board.copy())
        return

    for poss in range(8) :
        fail = False
        
        for priv in range(size) :
            if (poss == board[priv]) :
                fail = True
                break
            if (abs(poss - board[priv]) == abs(size - priv)) :
                fail = True
                break
        
        if (fail) :
            continue
        
        if(size < len(board)) :
            board[size] = poss
        else :
            board.append(poss)

        fill(board, size+1)

fill([], 0)

# -------------------------------------------------------------------

trueBoard_index = 0
for trueBoard in trueBoards :
    newBoard = [['O' for i in range(8)] for j in range(8)]
    
    for row in range(8) :
        newBoard[row][ trueBoard[row] ] = 'X'
    
    trueBoards[trueBoard_index] = newBoard
    trueBoard_index += 1

# --------------------------------------------------------------------

def print_gird(gird) :
    print('-------------------')
    for line in gird :
        print('| ', end='')
        for square in line :
            print(square+' ', end='')
        print('|')
    print('-------------------')

# ---------------------------------------------------------------------

myBoard = [['O' for i in range(8)] for j in range(8)]

for i in range(8) :
    row,column = tuple(map(int,(input().split(','))))
    myBoard[row-1][column-1] = 'X'

print_gird(myBoard)

dx = [0, 1, 0, -1, 1, 1, -1, -1]
dy = [-1, 0, 1, 0, -1, 1, 1, -1]

# ---------------------------------------------------------------------

def underThreat(board, row, column) :
    for i in range(8) :
        if  i == column :
            continue
        if(board[row][i] == 'X') :
            return True
    for i in range(8) :
        if i == row :
            continue
        if(board[i][column] == 'X') :
            return True
        if(column+row-i >= 0 and column+row-i < 8 and board[i][column+row-i] == 'X') :
            return True
        if(column-row+i >= 0 and column-row+i < 8 and board[i][column-row+i] == 'X') :
            return True
    return False

# ---------------------------------------------------------------------

dfs_step = 0

def dfs(myBoard, depth, depthLimit) :
    global dfs_step
    dfs_step += 1
#     print_gird(myBoard)
#     time.sleep(0.1)
    
    if myBoard in trueBoards :
        print('found in depth ',depth)
        print_gird(myBoard)
        return True

    if(depth >= depthLimit) :
        return False

    if (depth == len(visitedStates)) :
        visitedStates.append({""})
    visitedStates[depth].add(str(myBoard))

    for row in range(8) :
        for column in range(8) :
            if (myBoard[row][column] != 'X') :
                continue
            if (not underThreat(myBoard, row, column)) :
                continue

            for i in range(8) :
                if (row+dy[i] < 0 or row+dy[i] > 7 or column+dx[i] < 0 or column+dx[i] > 7) :
                    continue
                if (myBoard[row+dy[i]][column+dx[i]] == 'X') :
                    continue
                
                myBoard[row+dy[i]][column+dx[i]] = 'X'
                myBoard[row][column] = 'O'

                visited = False
                for d in range(depth+1) :
                    if str(myBoard) in visitedStates[d] :
                        visited = True
                        break
                
                if not visited :
                    if dfs(myBoard, depth+1, depthLimit) :
                        return True

                myBoard[row+dy[i]][column+dx[i]] = 'O'
                myBoard[row][column] = 'X'
                
# -----------------------------------------------------------------------------------

depthLimit = 0
myDFSboard = copy.deepcopy(myBoard)

while True :
    visitedStates = [{""}]
    if dfs(myDFSboard, 0, depthLimit) :
        break
    print('not found in depth '+str(depthLimit))
    depthLimit += 1
print(dfs_step,'step')
trueBoards = []

def fill(board, size) :
    if (size == 8) :
        trueBoards.append(board.copy())
        return

    for poss in range(8) :
        fail = False
        
        for priv in range(size) :
            if (poss == board[priv]) :
                fail = True
                break
            if (abs(poss - board[priv]) == abs(size - priv)) :
                fail = True
                break
        
        if (fail) :
            continue
        
        if(size < len(board)) :
            board[size] = poss
        else :
            board.append(poss)

        fill(board, size+1)

fill([], 0)

for i in trueBoards :
    print(i)
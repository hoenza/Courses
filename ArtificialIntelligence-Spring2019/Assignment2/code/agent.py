import math
import copy

class myNode:
    def __init__(self, from_cell, to_here_cell, board):
        self.children = []
        self.utility = 0
        self.decisionChild = None
        self.from_cell = from_cell
        self.to_here_cell = to_here_cell
        self.board = board

    def setChild(self, child):
        self.children.append(child)

    def setUtility(self, utility):
        self.utility = utility

    def setDecisionChild(self, decisionChild):
        self.decisionChild = decisionChild

    def getDecisionChild(self):
        return self.decisionChild

    def getFromCell(self):
        return self.from_cell

    def getToCell(self):
        return self.to_here_cell

    def setEvaluationFunction(self, myColor, opponentColor):
        if self.board.finishedGame() :
            self.utility = self.endValue(myColor, opponentColor)
            return
        evaluation = 0
        rowNumber = self.board.n_rows
        for row in range(rowNumber) :
            for cell in self.board.board[row] :
                if myColor == 'W':
                    evaluation += row+1 if cell == 'W' else row-rowNumber if cell == 'B' else 0
                else :
                    evaluation += rowNumber-row if cell == 'B' else -row-1 if cell == 'W' else 0
        self.utility = evaluation
    
    def endValue(self, myColor, opponentColor) :
        if self.board.win(myColor) :
            return math.inf
        if self.board.win(opponentColor) :
            return -math.inf
        return 0


class myTree:
    def __init__(self, board, color, opponentColor, height):
        self.height = height
        self.board = board
        self.nodes = [[] for i in range(self.height+1)]
        self.root = self.makeNode(0, board)

    def makeNode(self, height, board, from_cell=None, to_cell=None):
        node = myNode(from_cell, to_cell, board)
        self.nodes[height].append(node)
        return node

    def newBranch(self, node, i, j, color, piecesFromCell, piecesToCell) :
        newBoard = copy.deepcopy(node.board)
        newBoard.changePieceLocation(color, piecesFromCell[i], piecesToCell[i][j])
        child = self.makeNode(self.height, newBoard, piecesFromCell[i], piecesToCell[i][j])
        node.setChild(child)
        return child

def rowList(myColor, rowNumber) :
    if myColor == 'B' :
        return [i for i in range(rowNumber)]
    else :
        return [rowNumber-i-1 for i in range(rowNumber)]

def myGetPiecesPossibleLocations(node, color):
        piecesFromCell = []
        piecesToCell = []
        for i in rowList(color, node.board.n_rows):
            for j in range(node.board.n_cols):
                piecesPossibleLocations = []
                if node.board.board[i][j] == color:
                    piecesPossibleLocations = node.board.getPiecePossibeLocations(color, i, j)
                if len(piecesPossibleLocations) > 0:
                    piecesFromCell.append((i, j))
                    piecesToCell.append(piecesPossibleLocations)

        return piecesFromCell, piecesToCell

class alphaBet:
    @staticmethod
    def calNextMove(tree, myColor, opponentColor):
        alphaBet.computeAlphaBet(tree, tree.root, myColor, opponentColor, True, 0, math.inf)
        decistionNode = tree.root.getDecisionChild()
        if not decistionNode :
            decistionNode = tree.root.children[0]
        return decistionNode.getFromCell(), decistionNode.getToCell()

    @staticmethod
    def computeAlphaBet(tree, node, myColor, opponentColor, isMax, thisHeight, alpha) :
        if (thisHeight == tree.height) :
            node.setEvaluationFunction(myColor, opponentColor)
        elif node.board.finishedGame() :
            node.setUtility(node.endValue(myColor, opponentColor))
        else :
            value, decisionNode = alphaBet.searchInChildren(tree, node, myColor, opponentColor, isMax, thisHeight, alpha)
            node.setUtility(value)
            node.setDecisionChild(decisionNode)
        return node.utility

    @staticmethod
    def searchInChildren(tree, node, myColor, opponentColor, isMax, thisHeight, alpha) :
        alphaBetBroke = False
        moveColor = myColor if isMax else opponentColor
        decisionNode = None
        value = -math.inf if isMax else math.inf
        piecesFromCell, piecesToCell = myGetPiecesPossibleLocations(node, moveColor)
        for i in range(len(piecesToCell)):
            for j in range(len(piecesToCell[i])):
                child = tree.newBranch(node, i, j, moveColor, piecesFromCell, piecesToCell)
                newValue = alphaBet.computeAlphaBet(tree, child, myColor, opponentColor, not isMax, thisHeight+1, value)
                if (isMax and newValue > value) or (not isMax and newValue < value) :
                    value = newValue
                    decisionNode = child
                if (isMax and value >= alpha) or (not isMax and value <= alpha) :
                    alphaBetBroke = True
                    break
            if alphaBetBroke : break
        
        return value, decisionNode
        

class Agent:
    def __init__(self, myColor, opponentColor):
        self.myColor = myColor
        self.opponentColor = opponentColor
        self.height = 4

    def move(self, board):
        gameTree = myTree(board, self.myColor, self.opponentColor, self.height)
        from_cell, to_cell = alphaBet.calNextMove(gameTree, self.myColor, self.opponentColor)
        return from_cell, to_cell
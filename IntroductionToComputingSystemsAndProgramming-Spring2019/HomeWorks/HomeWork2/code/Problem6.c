#include <stdio.h>
#include <math.h>

int main() {
    int number;
    scanf("%d", &number);
    
    for (int i = 0; i <= number; i++) {
        for(int j = 0; j <= number; j++)
            printf("%d ", abs(j-i));
        printf("\n");
    }
    return 0;
}
#include <stdio.h>

int main() {
    int step, max;
    scanf("%d%d", &step, &max);
    for (int i = 1; i <= max; i++) {
        if (i%step)
            printf("%d ", i);
        else
            printf("Hop ");
    }
    return 0;
}
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define TRUE 1
#define FALSE 0
#define AGAIN TRUE
#define END_GAME FALSE
const int SLOTS_NUMBER = 4;
const int COLORS_NUMBER = 6;
const int GUESSES_NUMBER = 10;

struct SlotPack {
    int slot1;
    int slot2;
    int slot3;
    int slot4;
};

int* getSlot(struct SlotPack* slotPack, int index) {
    switch(index) {
        case 1: return &slotPack->slot1;
        case 2: return &slotPack->slot2;
        case 3: return &slotPack->slot3;
        case 4: return &slotPack->slot4;
        default: return 0;
    }
}




void showResult(int correctPosition, int correctColor) {
    printf("\n");
    printf("          You guessed %d position correctly!\n", correctPosition);
    printf("          You guessed %d colors correctly!\n\n\n", correctColor);
    printf("___________________________________________________\n\n");
}




int checkGuess(struct SlotPack guessDigits, struct SlotPack randomDigits) {
    int correctPosition = 0, correctColor = 0;
    struct SlotPack checkedGuessSlots, checkedrandomSlots;
    for(int i=1; i<=SLOTS_NUMBER; i++) {
        *getSlot(&checkedGuessSlots, i) = *getSlot(&checkedrandomSlots, i) = FALSE;
    }
    
    for(int i=1; i<=SLOTS_NUMBER; i++) {
        if(*getSlot(&guessDigits, i) == *getSlot(&randomDigits, i)) {
            correctPosition++;
            *getSlot(&checkedGuessSlots, i) = *getSlot(&checkedrandomSlots, i) = TRUE;
        }
    }

    for(int i=1; i<=SLOTS_NUMBER; i++) {
        if(*getSlot(&checkedGuessSlots, i)) continue;
        for(int j=1; j<=SLOTS_NUMBER; j++) {
            if(*getSlot(&checkedrandomSlots, j)) continue;
            if(*getSlot(&guessDigits, i) == *getSlot(&randomDigits, j)) {
                correctColor++;
                *getSlot(&checkedGuessSlots, i) = *getSlot(&checkedrandomSlots, j) = TRUE;
                break;
            }
        }
    }

    showResult(correctPosition, correctColor);
    return (correctPosition == SLOTS_NUMBER);
}




struct SlotPack readGuess() {
    int wrongInput;
    struct SlotPack guessDigits;

    do {
        wrongInput = FALSE;
        int guessNumber;
        scanf("%d", &guessNumber);

        guessDigits.slot4 = guessNumber % 10;
        guessNumber /= 10;
        guessDigits.slot3 = guessNumber % 10;
        guessNumber /= 10;
        guessDigits.slot2 = guessNumber % 10;
        guessNumber /= 10;
        guessDigits.slot1 = guessNumber % 10;
        guessNumber /= 10;

        if(guessDigits.slot4 > COLORS_NUMBER || guessDigits.slot3 > COLORS_NUMBER || guessDigits.slot2 > COLORS_NUMBER || guessDigits.slot1 > COLORS_NUMBER
        || guessDigits.slot4 < 1 || guessDigits.slot3 < 1 || guessDigits.slot2 < 1 || guessDigits.slot1 < 1
        || guessNumber > 0) {
            printf("number out of range!! try again\n\n");
            wrongInput = TRUE;
        }
    }while(wrongInput);

    return guessDigits;
}




struct SlotPack  randomPlacer() {
    struct SlotPack randomDigits;
    randomDigits.slot1 = rand() % COLORS_NUMBER + 1;
    randomDigits.slot2 = rand() % COLORS_NUMBER + 1;
    randomDigits.slot3 = rand() % COLORS_NUMBER + 1;
    randomDigits.slot4 = rand() % COLORS_NUMBER + 1;
    return randomDigits;
}




int calLastScore(int step) {
    return ( (GUESSES_NUMBER - step) * 100 );
}




void newGame(int* lastScore) {
    struct SlotPack randomDigits = randomPlacer();
    printf("___________________________________________________\n\n");
    printf("          I've chosen a number between [1111,6666]\n");
    printf("          Go on and guess :\n\n");
    
    int win = FALSE;
    for(int i=0; i<GUESSES_NUMBER; i++) {
        if(checkGuess(readGuess(), randomDigits)) {
            win = TRUE;
            *lastScore = calLastScore(i);
            break;
        }
    }

    if(!win) {
        *lastScore = 0;
        printf("          Bad job. you lose! your score :\t%d\n", *lastScore);
    } else {
        printf("          Prefect job. you win with score :\t%d\n", *lastScore);
    }
    printf("          my number was :\t%d%d%d%d\n\n", randomDigits.slot1, randomDigits.slot2, randomDigits.slot3, randomDigits.slot4);
    printf("___________________________________________________\n\n");
}




void showRecord(int* lastScore) {
    printf("___________________________________________________\n\n");
    if(lastScore < 0) {
        printf("          Nothing yet!\n");
    } else {
        printf("          Last score was :\t%d\n", *lastScore);
    }
}




int firstMenue(int* lastScore) {
    int firstTime = (*lastScore < 0);

    if(!firstTime)
        printf("___________________________________________________\n\n");
    printf("   ...::: Hello, Welcome to Master Mind ! :::...\n\n\n");
    printf("                 1. New Game\n");
    if (firstTime)
        printf("                 2. Quit\n");
    else {
        printf("                 2. Last Record\n");
        printf("                 3. Quit\n");
    }

    int choose;
    scanf("%d", &choose);

    if (choose == 1) {
        newGame(lastScore);
        return AGAIN;
    }
    else {
        if (!firstTime && choose == 2) {
            showRecord(lastScore);
            return AGAIN;
        } else {
            printf("___________________________________________________\n\n");
            printf("Good bye ...\n");
            return END_GAME;
        }
    }
}




int main() {
    srand(time(0));
    int lastScore = -1;
    while(firstMenue(&lastScore)) ;
}
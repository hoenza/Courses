#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define TRUE 1
#define FALSE 0
#define AGAIN TRUE
#define END_GAME FALSE
const int SLOTS_NUMBER = 4;
const int GUESSES_NUMBER = 10;




void showResult(int correctPosition, int correctColor) {
    printf("\n");
    printf("___________________________________________________\n\n");
    printf("          You guessed %d position correctly!\n", correctPosition);
    printf("          You guessed %d colors correctly!\n\n\n", correctColor);
}




int checkGuess(int* guessDigits, int* randomDigits) {
    int corretPosition = 0, corretColor = 0;
    int* checkedGuessSlots = malloc(SLOTS_NUMBER);
    int* checkedrandomSlots = malloc(SLOTS_NUMBER);
    for(int i=0; i<SLOTS_NUMBER; i++) {
        checkedGuessSlots[i] = FALSE;
        checkedrandomSlots[i] = FALSE;
    }
    for(int i=0; i<SLOTS_NUMBER; i++) {
        if(guessDigits[i] == randomDigits[i]) {
            corretPosition++;
            checkedGuessSlots[i] = TRUE;
            checkedrandomSlots[i] = TRUE;
        }
    }
    for(int i=0; i<SLOTS_NUMBER; i++) {
        if(checkedrandomSlots[i]) continue;
        for(int j=0; j<SLOTS_NUMBER; j++) {
            if(checkedGuessSlots[j]) continue;
            if(guessDigits[j] == randomDigits[i]) {
                corretColor++;
                checkedrandomSlots[i] = TRUE;
                checkedGuessSlots[j] = TRUE;
                break;
            }
        }
    }
    free(checkedGuessSlots);
    free(checkedrandomSlots);
    free(guessDigits);
    showResult(corretPosition, corretColor);
    return (corretPosition == SLOTS_NUMBER);
}




int* readGuess() {
    int guessNumber;
    scanf("%d", &guessNumber);
    int* guessDigits = malloc(SLOTS_NUMBER);
    for(int i=0; i<SLOTS_NUMBER; i++){
        guessDigits[SLOTS_NUMBER - i - 1] = guessNumber % 10;
        guessNumber /= 10;
    }
    return guessDigits;
}




int*  randomPlacer() {
    int* randomDigits = malloc(SLOTS_NUMBER);
    for(int i=0; i<SLOTS_NUMBER; i++) {
        randomDigits[i] = rand() % 6 + 1;
    }
    return randomDigits;
}




int calLastScore(int step) {
    return ( (10 - step) * 100 );
}




void newGame(int* lastScore) {
    int* randomDigits = randomPlacer();
    printf("___________________________________________________\n\n");
    printf("          I've chosen a number between [1111,6666]\n");
    printf("          Go on and guess :\n\n");

    int win = FALSE;
    for(int i=0; i<GUESSES_NUMBER; i++) {
        if(checkGuess(readGuess(), randomDigits)) {
            win = TRUE;
            *lastScore = calLastScore(i);
            break;
        }
    }

    printf("___________________________________________________\n\n");
    if(!win) {
        *lastScore = 0;
        printf("          Bad job. you lose! your score :\t%d\n", *lastScore);
    } else {
        printf("          Prefect job. you win with score :\t%d\n", *lastScore);
    }
    printf("          my number was :\t");
    for(int i=0; i<SLOTS_NUMBER; i++){
        printf("%d", randomDigits[i]);
    }
    printf("\n\n");
    free(randomDigits);
}




void showRecord(int* lastScore) {
    printf("___________________________________________________\n\n");
    if(lastScore < 0) {
        printf("          Nothing yet!\n");
    } else {
        printf("          Last score was :\t%d\n", *lastScore);
    }
}




int firstMenue(int* lastScore) {
    int firstTime = (*lastScore < 0);

    if(!firstTime)
        printf("___________________________________________________\n\n");
    printf("   ...::: Hello, Welcome to Master Mind ! :::...\n\n\n");
    printf("                 1. New Game\n");
    if (firstTime)
        printf("                 2. Quite\n");
    else {
        printf("                 2. Last Record\n");
        printf("                 3. Quite\n");
    }

    int choose;
    scanf("%d", &choose);

    if (choose == 1) {
        newGame(lastScore);
        return AGAIN;
    }
    else {
        if (!firstTime && choose == 2) {
            showRecord(lastScore);
            return AGAIN;
        } else {
            printf("___________________________________________________\n\n");
            printf("Good bye ...\n");
            return END_GAME;
        }
    }
}




int main() {
    srand(time(0));
    int lastScore = -1;
    while(firstMenue(&lastScore)) ;
}
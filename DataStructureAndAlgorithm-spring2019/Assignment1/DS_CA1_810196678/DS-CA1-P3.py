size = int(input())
string = input()

class perInf :
    def __init__(self, A, C, G, T):
        self.Anum = A
        self.Cnum = C
        self.Gnum = G
        self.Tnum = T

perInfs = []
perInfs.append(perInf(0,0,0,0))

for index in range(0, size) :
    if (string[index] == 'A') : perInfs.append(perInf(perInfs[index].Anum+1, perInfs[index].Cnum, perInfs[index].Gnum, perInfs[index].Tnum))
    if (string[index] == 'C') : perInfs.append(perInf(perInfs[index].Anum, perInfs[index].Cnum+1, perInfs[index].Gnum, perInfs[index].Tnum))
    if (string[index] == 'G') : perInfs.append(perInf(perInfs[index].Anum, perInfs[index].Cnum, perInfs[index].Gnum+1, perInfs[index].Tnum))
    if (string[index] == 'T') : perInfs.append(perInf(perInfs[index].Anum, perInfs[index].Cnum, perInfs[index].Gnum, perInfs[index].Tnum+1))

extra = perInf(
    perInfs[size].Anum - int(size/4),
    perInfs[size].Cnum - int(size/4),
    perInfs[size].Gnum - int(size/4),
    perInfs[size].Tnum - int(size/4)
)

answerPeriod = {'start': 1, 'end': size, 'length': size}

end = 0
for start in range(1, size+1) :
    while (end <= size) :
        inInf = perInf(
            perInfs[end].Anum - perInfs[start-1].Anum,
            perInfs[end].Cnum - perInfs[start-1].Cnum,
            perInfs[end].Gnum - perInfs[start-1].Gnum,
            perInfs[end].Tnum - perInfs[start-1].Tnum
        )
        if(inInf.Anum >= extra.Anum and inInf.Cnum >= extra.Cnum and inInf.Gnum >= extra.Gnum and inInf.Tnum >= extra.Tnum) :
            if(end - start + 1 < answerPeriod['length']) :
                answerPeriod = {'start': start, 'end': end, 'length': end-start+1}
            break
        end = end+1

print(answerPeriod['length'])
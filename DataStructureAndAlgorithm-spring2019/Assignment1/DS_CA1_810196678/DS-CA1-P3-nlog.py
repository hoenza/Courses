size = int(input())
string = input()

class perInf :
    def __init__(self, A, C, G, T):
        self.Anum = A
        self.Cnum = C
        self.Gnum = G
        self.Tnum = T

perInfs = []
perInfs.append(perInf(0,0,0,0))

for index in range(0, size) :
    if (string[index] == 'A') : perInfs.append(perInf(perInfs[index].Anum+1, perInfs[index].Cnum, perInfs[index].Gnum, perInfs[index].Tnum))
    if (string[index] == 'C') : perInfs.append(perInf(perInfs[index].Anum, perInfs[index].Cnum+1, perInfs[index].Gnum, perInfs[index].Tnum))
    if (string[index] == 'G') : perInfs.append(perInf(perInfs[index].Anum, perInfs[index].Cnum, perInfs[index].Gnum+1, perInfs[index].Tnum))
    if (string[index] == 'T') : perInfs.append(perInf(perInfs[index].Anum, perInfs[index].Cnum, perInfs[index].Gnum, perInfs[index].Tnum+1))

def binSearch(start, end, binStep) :
    outInf = perInf(
        perInfs[size].Anum - (perInfs[end-1].Anum - perInfs[start-1].Anum),
        perInfs[size].Cnum - (perInfs[end-1].Cnum - perInfs[start-1].Cnum),
        perInfs[size].Gnum - (perInfs[end-1].Gnum - perInfs[start-1].Gnum),
        perInfs[size].Tnum - (perInfs[end-1].Tnum - perInfs[start-1].Tnum)
    )
    
    if(end > size) : return {'start': 0, 'end': size}

    if (binStep == 0) :
        if(outInf.Anum > size/4 or outInf.Cnum > size/4 or outInf.Gnum > size/4 or outInf.Tnum > size/4) :
            return binSearch(start, end+1, 0)
        else :
            return {'start': start, 'end': end}

    if(outInf.Anum > size/4 or outInf.Cnum > size/4 or outInf.Gnum > size/4 or outInf.Tnum > size/4) :
        return binSearch(start, int(end+binStep/2), int(binStep/2))
    else :
        return binSearch(start, int(end-binStep/2), int(binStep/2))

minSize = size
for start in range(1, size+1) :
    period = binSearch(start, int((size+start)/2), (size-start+1)/2)
    periodSize = period['end'] - period['start']
    if ( periodSize < minSize ) :
        minSize = periodSize

if (minSize == -1): minSize = 0
print(minSize)
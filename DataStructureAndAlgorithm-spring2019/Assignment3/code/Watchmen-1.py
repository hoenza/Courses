n = int(input())
heights = [ int(height) for height in input().split(' ') ]
rightTargets = [ i for i in range(n) ]
leftTargets = [ i for i in range(n) ]

index = 0
while index < n :
    reachEnd = True
    for target in range(index+1, n) :
        if(heights[target] > heights[index]) :
            for i in range(index, target) :
                rightTargets[i] = target
                print("rightTargets[",i,"] = ",target)
            index = target
            reachEnd = False
            break
    if reachEnd :
        for i in range(index, n) :
            rightTargets[i] = n - 1
            print("last rightTargets[",i,"] = ",n - 1)
        break

index = n-1
while index >= 0 :
    for target in reversed(range(0, index)) :
        if(heights[target] > heights[index]) :
            for i in reversed(range(index, target)) :
                leftTargets[i] = target
            index = target + 1
            break
    for i in range(0, index+1) :
        leftTargets[i] = 0
    index -= 1

for index in range(n) :
    maxDistances = max(rightTargets[index]-index, index-leftTargets[index])
    print(maxDistances, end=' ')
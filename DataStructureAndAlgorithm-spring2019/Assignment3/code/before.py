n = int(input())
heights = [ int(height) for height in input().split(' ') ]

def vision(index, dir) :
    if(dir == "right") :
        for target in range(index+1, n) :
            if(heights[target] > heights[index]) :
                return target - index
        return n - 1 - index
    elif(dir == "left") :
        for target in reversed(range(0, index)) :
            if(heights[target] > heights[index]) :
                return index - target
        return index

for index in range(n) :
    leftDistance = vision(index, "left")
    rightDistance = vision(index, "right")
    maxDistances = max(leftDistance, rightDistance)
    print(maxDistances, end=' ')
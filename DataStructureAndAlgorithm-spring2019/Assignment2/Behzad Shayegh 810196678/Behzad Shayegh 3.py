n = int(input())
words = [ word.lower() for word in input().split(' ') ]

minLen = len(words[0])
for word in words :
    if len(word) < minLen :
        minLen = len(word)

def findPrefixIndex(words, length, index) :
    if index == length :
        return index
    prefix = True
    for word in words :
        if word[index] != words[0][index] :
            prefix = False
            break
    if prefix :
        return findPrefixIndex(words, length, index+1)
    else :
        return index

PrefixIndex = findPrefixIndex(words, minLen, 0)
if PrefixIndex == 0 :
    print('NULL')
else :
    print(words[0][:PrefixIndex])
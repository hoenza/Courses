n = int(input())
balaBarA = [i+1 for i in range(n)]
balaBarB = []
balaBarC = []

def realName(balaBarNum) :
    return 'A' if id(balaBarNum) == id(balaBarA) else 'B' if id(balaBarNum) == id(balaBarB) else 'C'

def transfer(n, balaBar1, balaBar2, balaBar3):
    if n > 0:
        transfer(n - 1, balaBar1, balaBar3, balaBar2)
        if balaBar1:
            balaBar3.append(balaBar1.pop())
            print(realName(balaBar1), realName(balaBar3))
        transfer(n - 1, balaBar2, balaBar1, balaBar3)

transfer(n, balaBarA, balaBarB, balaBarC)
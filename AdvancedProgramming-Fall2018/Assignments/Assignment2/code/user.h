#ifndef USER_H
#define USER_H

#include "unity.h"
using namespace std;

class User{
public:
	enum Role{
		BUYER,
		SELLER,
		BUYER_SELLER,
		WRONG
	};
	User();
	bool same_username(string);
	bool same_username(User*);
	int find_user_index(const vector<User*>);
	bool exist_in(vector<User*>);
	bool ready_to_change_role(const User*);
	void change_role_to_buyerseller(User*);
	Role return_Role();
	bool smaller_in_username_than(User*);
	void decrease(double);
	string userName();
	bool enough_money(double);
	void raise_money(double);
	string return_a_line_for_factor();
	Role Read_Role();

	vector<int> factors_number;
	
private:
	string first_name;
	string last_name;
	string username;
	string phone_number;
	double money;
	Role role;
};

#endif
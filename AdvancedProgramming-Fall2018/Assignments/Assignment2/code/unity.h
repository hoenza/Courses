#ifndef UNITY_H
#define UNITY_H

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
using namespace std;

void Failed();
void OK();
string to_string(int);
string to_string(double);
void print_one_factor(vector<string>);

#endif
#ifndef GOOD_H
#define GOOD_H

#include "unity.h"
#include "user.h"

using namespace std;

class Good{
public:
	Good(User*);
	Good(Good*,int);
	bool same_name(string);
	bool same_seller(Good*);
	int find_good_index(const vector<Good*>);
	bool exist_in(vector<Good*>);
	bool there_is(string,int);
	bool smaller_for_show(Good*);
	bool enough_count(int);
	double bought(int);
	bool is_not_avalable_in_more();
	User* use_seller();
	void calculate_prices(double&,int);
	bool requested_for_sale(string);
	void add_to_sale_requesters(string);
	void check_saled_for_requests();
	bool bestseller();
	void make_special(int);
	void delete_special(int);
	int special_count();
	int count_of_this_good_in(vector<Good*>);
	void check_saled_for_much_count(vector<Good*>,double);
	bool saled_position();
	string return_a_line_for_factor(int,int);
	bool return_saled_of_market();
	friend ostream& operator<<(ostream&,const Good&);

private:
	User* seller;
	string name;
	double price;
	int count;
	int special_number;
	int traded_numtimes;	
	vector<string> sale_requesters;
	bool saled;
	bool saled_of_market;
};

#endif
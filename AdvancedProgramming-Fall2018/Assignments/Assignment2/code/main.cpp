#include <iostream>
#include "unity.h"
#include "market.h"
using namespace std;


int main(){
	Market MARKET;
	
	string Command;
	while(cin >> Command){
		if(Command == "add_user")
			MARKET.Add_user(new User());
		else if(Command == "add_goods")
			MARKET.Add_goods();
		else if(Command == "search")
			MARKET.Search();
		else if(Command == "buy")
			MARKET.buy();
		else if(Command == "sale")
			MARKET.sale();
		else if(Command == "add_special")
			MARKET.add_special();
		else if(Command == "remove_special")
			MARKET.remove_special();
		else if(Command == "search_sale")
			MARKET.search_sale();
		else if(Command == "search_bestseller")
			MARKET.search_bestseller();
		else if(Command == "add_money")
			MARKET.add_money();
		else if(Command == "request")
			MARKET.request();
		else if(Command == "print_request")
			MARKET.print_request();
		else if(Command == "print_factor")
			MARKET.print_factor();
		else if(Command == "Qxit")
			break;
		else Failed();
	}
	return 0;
}
#ifndef FACTOR_H
#define FACTOR_H

#include "unity.h"
using namespace std;

class Factor{
public:
	Factor(vector<string>);
	void print_one_factor();

private:
	vector<string> paper;
};

#endif
#include "request.h"

Request::Request(string _name,User* _requester){
	name = _name;
	requester = _requester;
	
	string goods_name_, seller_username_;
	int count_;
	bool again;
	do{
		cin >> goods_name_ >> count_ >> seller_username_;
		again = (seller_username_[seller_username_.size()-1]==',');
		seller_username_.erase(seller_username_.size()-1);
			this->goods_name.push_back(goods_name_);
			this->count.push_back(count_);
			this->seller_username.push_back(seller_username_);
	}while(again);
}
bool Request::same_name(string other_name){
	return (this->name == other_name);
}
bool Request::same_requester(string requester_username){
	return (this->requester->same_username(requester_username));
}
bool Request::same_requester(User* _requester){
	return (this->requester->same_username(_requester));
}
void Request::print_this_request(int number){
	cout << "Request " << number << " " << this->name << " " << this->requester->userName() << endl;
	for(int i=0; i<goods_name.size(); i++){
		cout << i+1 << ". " << goods_name[i] << " " << count[i] << " " << seller_username[i] << endl;
	}
}

User* Request::Get_requester(){
	return requester;
}
vector<string> Request::Get_goods_name(){
	return goods_name;
}
vector<int> Request::Get_count(){
	return count;
}
vector<string> Request::Get_seller_username(){
	return seller_username;
}
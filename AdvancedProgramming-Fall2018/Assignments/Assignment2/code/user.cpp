#include "user.h"

User::User(){
	cin >> first_name >> last_name >> username >> phone_number >> money;
	role = Read_Role();
}

bool User::same_username(string _username){
	return this->username == _username;
}
bool User::same_username(User* other_user){
	return this->same_username(other_user->username);
}
int User::find_user_index(const vector<User*> users){
	for(int i=0; i<users.size(); i++)
		if(users[i]->same_username(this->username))
			return i;
	return -1;
}
bool User::exist_in(vector<User*> users){
	return (find_user_index(users)>=0);
}
bool User::ready_to_change_role(const User* other_user){
	return (this->role != BUYER_SELLER && this->role != other_user->role
			&& this->first_name == other_user->first_name
			&& this->last_name == other_user->last_name
			&& this->phone_number == other_user->phone_number
			&& this->username == other_user->username);
}
void User::change_role_to_buyerseller(User* new_user){
	this->money += new_user->money;
	this->role = BUYER_SELLER;
	delete new_user;
}
User::Role User::return_Role(){
	return this->role;
}
bool User::smaller_in_username_than(User* that){
	this->username < that->username;
}
void User::decrease(double price){
	this->money -= price;
}
string User::userName(){
	return username;
}
bool User::enough_money(double money_need){
	return (this->money >= money_need);
}
void User::raise_money(double plus_money){
	this->money += plus_money;
}
string User::return_a_line_for_factor(){
	string line="";
	line += (this->first_name+" "+this->last_name+" "+this->phone_number+"\n");
	return line;
}

User::Role User::Read_Role(){
	string role;
	cin >> role;
	if(role == "buyer")
		return BUYER;
	if(role == "seller")
		return SELLER;
	else return WRONG;
}
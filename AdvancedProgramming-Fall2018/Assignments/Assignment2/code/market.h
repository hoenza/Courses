#ifndef MARKET_H
#define MARKET_H

#include "unity.h"
#include "user.h"
#include "good.h"
#include "request.h"
#include "factor.h"
using namespace std;

class Market{
public:
	Market();
	void Add_user(User*);
	void Add_goods();
	void Search();
	void buy();
	void sale();
	void add_special();
	void remove_special();
	void search_sale();
	void search_bestseller();
	void add_money();
	void request();
	void print_request();
	void print_one_factor(User*,vector<Good*>,vector<int>,double);
	void print_factor();
	void handle_buying(User*,vector<string>,vector<int>,vector<string>,double);

	int find_user_index(string);
	bool exist_in_as(string,User::Role);
	bool exist_in(string);
	int find_good_index(string,string);
	int find_good_index(string,User*);
	vector<Good*> find_goods(string,int);
	vector<Good*> find_saled_goods();
	vector<Good*> find_bestseller_goods();
	void sort_goods(vector<Good*>&);
	bool handle_request(Request*);
	
private:
	vector<User*> users;
	vector<Good*> goods;
	vector<Request*> requests;
	vector<Factor*> factors;
	double Market_money;
};

#endif
#include "myExeptions.h"
using namespace std;

void UnknownCommand::print_error() {
	cout << "Unknown command!" << endl;
}

void UnExpectedCommand::print_error() {
	cout << "Unexpected command!" << endl;
}

void RepetitiveRoom::print_error() {
	cout << "Repetitive room!" << endl;
}

void MoreMafiaRolesThanNumberOfMafias::print_error() {
	cout << "More mafia roles than number of mafias!" << endl;
}

void MoreVillagerRolesThanNumberOfVillagers::print_error() {
	cout << "More villager roles than number of villagers!" << endl;
}

void UnknownRole::print_error() {
	cout << "Unknown role!" << endl;
}

void _InvalidRoomName_::print_error() {
	cout << "Invalid room name" << endl;
}

void RepetitivePlayer::print_error() {
	cout << "Repetitive player!" << endl;
}

void _ManyUsers_::print_error() {
	cout << "many users" << endl;
}

void _UserNotJoined_::print_error() {
	cout << "User not joined" << endl;
}

void _ThisUserHasBeenSilencedBefor_::print_error() {
	cout << "This user has been dilenced befor" << endl;
}

void _UserAlreadyDied_::print_error() {
	cout << "User already died" << endl;
}

void _ThisUserCanNotVote_::print_error() {
	cout << "This user can not vote" << endl;
}

void _DetectiveHasAlreadyAsked_::print_error() {
	cout << "Detective has already asked" << endl;
}

void _UserCanNotAsk_::print_error() {
	cout << "User can not ask" << endl;
}

void _PersonIsDead_::print_error() {
	cout << "Person is dead" << endl;
}

void _DoctorHasAlreadyHealed_::print_error() {
	cout << "Doctor has already healed" << endl;
}

void _UserCanNotHeal_::print_error() {
	cout << "User can not heal" << endl;
}

void _SilencerHasAlreadySilenced_::print_error() {
	cout << "Silencer has already silenced" << endl;
}

void _UserCanNotSilence_::print_error() {
	cout << "User can not silence" << endl;
}

void NeedNumberOfRole::print_error() {
	cout << "Need number of role!" << endl;
}

void NumberOfRoleCanntBeLessThan0::print_error() {
	cout << "Number of role can not be less than 0!" << endl;
}

void RoomNeedName::print_error() {
	cout << "Room need name!" << endl;
}

void NeedVoterAndVotee::print_error() {
	cout << "Need voter and votee!" << endl;
}

void NeedDetectiveAndSuspect::print_error() {
	cout << "Need detective and suspect!" << endl;
}

void NeedDoctorAndPerson::print_error() {
	cout << "Need doctor and person!" << endl;
}

void NeedSilencerAndPerson::print_error() {
	cout << "Need silencer and person!" << endl;
}

void ChooseARoom::print_error() {
	cout << "Choose a room!" << endl;
}
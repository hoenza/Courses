#ifndef VILLAGER_h
#define VILLAGER_h

#include "Player.h"

class Villager : public Player {

public:
	Villager(std::string);
	~Villager();

	bool is_mafia();
	Player::Role get_role();
	Player::Team get_team();	
};

#endif
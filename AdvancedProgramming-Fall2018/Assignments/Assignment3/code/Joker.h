#ifndef JOKER_H
#define JOKER_H

#include "Player.h"

class Joker : public Player {

public:
	Joker(std::string);
	~Joker();

	void make_vote(Player*, bool);
	bool is_mafia();

	Player::Role get_role();
	Player::Team get_team();
};

#endif
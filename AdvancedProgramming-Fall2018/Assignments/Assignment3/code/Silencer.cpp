#include "Silencer.h"

Silencer::Silencer(std::string _name) : Mafia(_name) { this_night_silence = false; }

void Silencer::make_silence(Player* target){
	if(this_night_silence)
		throw new _SilencerHasAlreadySilenced_();
	else {
		this_night_silence = true;
		target->go_to_silence();
	}
}

void Silencer::recovery() {
	Player::recovery();
	this_night_silence = false;
}

bool Silencer::get_this_night_silence() { return this_night_silence; }

Player::Role Silencer::get_role() { return Player::SILENCER; }
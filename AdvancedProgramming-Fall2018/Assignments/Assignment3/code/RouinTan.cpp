#include "RouinTan.h"

RouinTan::RouinTan(std::string _name) : Villager(_name) { first_die = true; }

void RouinTan::die(bool night_time){
	if(night_time && first_die)
		first_die = false;
	else{
		Player::die(night_time);
	}
}

Player::Role RouinTan::get_role() { return Player::ROUINTAN; }
#include "Detective.h"

Detective::Detective(std::string _name) : Villager(_name) { this_night_detected = false; }

bool Detective::detect(Player* target) {
	if(this_night_detected)
		throw new _DetectiveHasAlreadyAsked_();
	else {
		this_night_detected = true;
		return target->is_mafia();
	}
}

void Detective::recovery() {
	Player::recovery();
	this_night_detected = false;
}

bool Detective::get_this_night_detected() { return this_night_detected; }

Player::Role Detective::get_role() { return Player::DETECTIVE; }
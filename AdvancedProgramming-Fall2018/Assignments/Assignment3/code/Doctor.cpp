#include "Doctor.h"

Doctor::Doctor(std::string _name) : Villager(_name) { this_night_healed = false; }

void Doctor::save(Player* target) {
	if(this_night_healed)
		throw new _DoctorHasAlreadyHealed_();
	else {
		this_night_healed = true;
		target->go_to_heal();
	}
}

void Doctor::recovery() {
	Player::recovery();
	this_night_healed = false;
}

bool Doctor::get_this_night_healed() { return this_night_healed; }

Player::Role Doctor::get_role() { return Player::DOCTOR; }
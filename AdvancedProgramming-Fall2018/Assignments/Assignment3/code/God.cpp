#include "God.h"

God::God() { running_room = NULL; };

void God::add_room(Room* new_room){
	if(exist_room(new_room)) throw new RepetitiveRoom();

	rooms.push_back(new_room);
	running_room = rooms[rooms.size()-1];
}

void God::switch_room(std::string room_name){
	for(int i=0; i<rooms.size(); i++)
		if(rooms[i]->same_room_name(room_name)){
			running_room = rooms[i];
			return;
		}
	throw new _InvalidRoomName_();
}

void God::finish_day() { 
	if(!running_room->next_time()){
		for(int i=0; i<rooms.size(); i++)
			if(running_room->same_room(rooms[i])) {
				rooms.erase(rooms.begin()+i);
				break;
			}
		delete running_room;
		running_room = NULL;
	}
}

void God::vote(std::string voter, std::string votee) { running_room->vote(voter,votee); }

void God::detect(std::string detective, std::string suspect) { running_room->detect(detective,suspect); }

bool God::exist_room(Room* new_room){
	for(int i=0; i<rooms.size(); i++)
		if(rooms[i]->same_room(new_room))
			return true;
		return false;
}

void God::join_player_to_this_room(std::string username){ running_room->join_player(username); }

void God::list_players() { running_room->list_players(); }

bool God::expected_command(std::string enter_command){
	return (enter_command=="Create_room"
	 		|| enter_command=="Switch_room"
			|| ((running_room!=NULL) && running_room->command_need() == enter_command)
			|| ((running_room!=NULL) && enter_command=="End_vote" && !running_room->get_night_time())
		);
}

void God::heal(std::string doctor, std::string person) { running_room->heal(doctor,person); }

bool God::is_night_finish() { return ( (running_room->get_night_time()) && (running_room->command_need()=="end_night") ); }

void God::finish_night_if_have_to() {
	if(running_room == NULL) return;

	if(is_night_finish()){
		if(!running_room->next_time()){
			delete running_room;
			running_room = NULL;
		}
	}
}

void God::silent(std::string silencer, std::string person) { running_room->silent(silencer,person); }

void God::get_room_state() {
	if(running_room == NULL)
		throw new ChooseARoom();

	running_room->print_state();
}
#include "Player.h"

Player::Player(std::string _name) : name(_name) {
	alive = true;
	silence = false;
	healed = false;
	incoming_votes = 0;
	vote_target = NULL;
}

Player::~Player() {}

void Player::go_to_silence() { silence = true; }

void Player::go_to_heal() { healed = true; }

bool Player::get_alive_status() { return alive; }

int Player::get_incoming_votes() { return incoming_votes; }

void Player::voting(Player* target) { vote_target = target; }

void Player::incoming_votes_inc() { incoming_votes++; }

std::string Player::get_username() { return name; }

bool Player::same_name(std::string _name) { return _name == name; }

bool Player::same_player(Player* _player) { return _player->name == name; }

void Player::make_vote(Player* target, bool night_time){
	if(silence)
		throw new _ThisUserHasBeenSilencedBefor_();
	if(night_time)
		throw new _ThisUserCanNotVote_();
	else
		voting(target);
}

void Player::clean_vote() { vote_target = NULL; }

bool Player::made_vote() { return vote_target != NULL; }

void Player::die(bool night_time) {	alive = false; }

bool Player::get_healed_status() { return healed; }

void Player::apply_vote() { vote_target->incoming_votes_inc(); }

void Player::recovery() {
	incoming_votes = 0;
	vote_target = NULL;
	healed = false;
}

void Player::silencing_recovery() { silence = false; }

bool Player::get_this_night_detected() { throw new _UserCanNotAsk_(); }
bool Player::get_this_night_healed() { throw new _UserCanNotHeal_(); }
bool Player::get_this_night_silence() { throw new _UserCanNotSilence_(); }
bool Player::detect(Player* target) { throw new _UserCanNotAsk_(); }
void Player::save(Player*) { throw new _UserCanNotHeal_(); }
void Player::make_silence(Player*) { throw new _UserCanNotSilence_(); }
#ifndef GODFATHER_H
#define GODFATHER_H

#include "Mafia.h"

class GodFather : public Mafia {

public:
	GodFather(std::string);
	~GodFather();
	
	bool is_mafia();
	Player::Role get_role();
};

#endif
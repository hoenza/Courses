#include "GodFather.h"

GodFather::GodFather(std::string _name) : Mafia(_name) {}

bool GodFather::is_mafia() { return false; }

Player::Role GodFather::get_role() { return Player::GODFATHER; }
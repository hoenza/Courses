#include "Mafia.h"

Mafia::Mafia(std::string _name) : Player(_name) {}

void Mafia::make_vote(Player* target, bool night_time) { voting(target); }

bool Mafia::is_mafia() { return true; }

Player::Role Mafia::get_role() { return Player::SIMPLE_MAFIA; }

Player::Team Mafia::get_team() { return Player::MAFIA; }
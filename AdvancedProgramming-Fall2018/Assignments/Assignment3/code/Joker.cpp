#include "Joker.h"

Joker::Joker(std::string _name) : Player(_name) {}

void Joker::make_vote(Player* target, bool night_time){
	if(night_time)
		throw new _ThisUserCanNotVote_();
	else
		target->incoming_votes_inc();
}

bool Joker::is_mafia() { return false; }

Player::Role Joker::get_role() { return Player::JOKER; }

Player::Team Joker::get_team() { return Player::SINGLE_JOKER; }
#ifndef DETECTIVE_H
#define DETECTIVE_H

#include "Villager.h"

class Detective : public Villager {

public:
	Detective(std::string);
	~Detective();
	
	bool detect(Player*);
	void recovery();
	
	bool get_this_night_detected();
	Player::Role get_role();
private:
	bool this_night_detected;

};

#endif
#ifndef SILENCER_H
#define SILENCER_H

#include "Mafia.h"

class Silencer : public Mafia {

public:
	Silencer(std::string);
	~Silencer();
	
	void make_silence(Player*);
	void recovery();

	bool get_this_night_silence();
	Player::Role get_role();
private:
	bool this_night_silence;

};

#endif
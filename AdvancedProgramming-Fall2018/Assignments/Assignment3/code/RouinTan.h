#ifndef ROUINTAN_H
#define ROUINTAN_H

#include "Villager.h"

class RouinTan : public Villager {

public:
	RouinTan(std::string);
	~RouinTan();

	void die(bool);
	
	Player::Role get_role();
private:
	bool first_die;
};

#endif
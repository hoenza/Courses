#include "Villager.h"

Villager::Villager(std::string _name) : Player(_name) {}

bool Villager::is_mafia() { return false; }

Player::Role Villager::get_role() { return Player::SIMPLE_VILLAGER; }

Player::Team Villager::get_team() { return Player::VILLAGER; }
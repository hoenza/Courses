#include "Interface.h"
using namespace std;

Interface::Interface() { god = God(); }

void Interface::run() {
	string line;
	while ( getline(cin,line) ) {
		istringstream words(line);

		try{
			in_try(words);
		}
		catch(My_Exeptions* e) { e->print_error(); }

		god.finish_night_if_have_to();
	}
}

void Interface::in_try(istringstream& words) {
	string enter_command;
	words >> enter_command;
		if(enter_command == "Get_room_state") {
			god.get_room_state();
		}
		else if(god.expected_command(enter_command))
			switch(command_reader(enter_command)){
				case Create_room:
					create_room(words);
					break;
				case Switch_room:
					switch_room(words);
					break;
				case Join:
					join(words);
					break;
				case Vote:
					vote(words);
					break;
				case End_vote:
					end_vote();
					break;
				case Detect:
					detect(words);
					break;
				case Heal:
					heal(words);
					break;
				case Silent:
					silent(words);
					break;
			}
		else
		 	throw new UnExpectedCommand();
}

void Interface::silent(istringstream& words) {
	string silencer, person;
	if(words >> silencer >> person) {
		god.silent(silencer,person);
	}
	else throw new NeedSilencerAndPerson();
}

void Interface::heal(istringstream& words){
	string doctor, person;
	if(words >> doctor >> person) {
		god.heal(doctor,person);
	}
	else throw new NeedDoctorAndPerson();
}

void Interface::detect(istringstream& words) {
	string detective, suspect;
	if(words >> detective >> suspect) {
		god.detect(detective,suspect);
	}
	else throw new NeedDetectiveAndSuspect();
}

void Interface::end_vote() { god.finish_day(); }

void Interface::vote(istringstream& words){
	string voter, votee;
	if(words >> voter >> votee){
		god.vote(voter,votee);
	}
	else throw new NeedVoterAndVotee();
}

Interface::Command Interface::command_reader(string enter_command){
	if(enter_command == "Create_room")
		return Create_room;
	if(enter_command == "Switch_room")
		return Switch_room;
	if(enter_command == "Join")
		return Join;
	if(enter_command == "Vote")
		return Vote;
	if(enter_command == "End_vote")
		return End_vote;
	if(enter_command == "Detect")
		return Detect;
	if(enter_command == "Heal")
		return Heal;
	if(enter_command == "Silent")
		return Silent;
	else
		throw new UnknownCommand();
}

void Interface::create_room(istringstream& words){
	string room_name;
	vector<Player::Role> roles;
	
	if(words >> room_name){
		read_roles(words,roles);
		check_roles(roles);

		god.add_room(new Room(room_name, roles));
	}
	else throw new RoomNeedName();
}

void Interface::clean_roles(vector<Player::Role>& roles, int speciall_villagers, int speciall_mafias){
	for(int i=roles.size()-1; i>=0 && speciall_villagers>0; i--)
		if(roles[i] == Player::SIMPLE_VILLAGER){
			roles.erase(roles.begin()+i);
			speciall_villagers--;
		}
	for(int i=roles.size()-1; i>=0 && speciall_mafias>0; i--)
		if(roles[i] == Player::SIMPLE_MAFIA){
			roles.erase(roles.begin()+i);
			speciall_mafias--;
		}
}

void Interface::check_roles(vector<Player::Role>& roles){
	int villagers=0, speciall_villagers=0, mafias=0, speciall_mafias=0;

	for(int i=0; i<roles.size(); i++)
		switch(roles[i]){
			case Player::JOKER:
				break;
			case Player::SIMPLE_VILLAGER:
				villagers++;
				break;
			case Player::SIMPLE_MAFIA:
				mafias++;
				break;
			case Player::DETECTIVE:
				speciall_villagers++;
				break;
			case Player::DOCTOR:
				speciall_villagers++;
				break;
			case Player::ROUINTAN:
				speciall_villagers++;
				break;
			case Player::GODFATHER:
				speciall_mafias++;
				break;
			case Player::SILENCER:
				speciall_mafias++;
				break;
		}
	if(speciall_villagers > villagers) throw new MoreVillagerRolesThanNumberOfVillagers();
	if(speciall_mafias > mafias) throw new MoreMafiaRolesThanNumberOfMafias();
	clean_roles(roles, speciall_villagers, speciall_mafias);
}

void Interface::read_roles(istringstream& words, vector<Player::Role>& roles){
	string role;
	int number_of_role;

	while(words >> role){
		if(words >> number_of_role){
			if(number_of_role>=0){
				Player::Role new_role = role_reader(role);
				clean_same_roles(new_role,roles);
				for(int i=0; i<number_of_role; i++)
					roles.push_back(new_role);
			}
			else throw new NumberOfRoleCanntBeLessThan0();
		}
		else throw new NeedNumberOfRole();
	}
}

void Interface::clean_same_roles(Player::Role new_role ,vector<Player::Role>& roles){
	for(int i=roles.size()-1; i>=0; i--)
		if(roles[i] == new_role)
			roles.erase(roles.begin()+i);
}

void Interface::switch_room(istringstream& words){
	string room_name;
	if(words >> room_name)
		god.switch_room(room_name);

	else throw new RoomNeedName();
}

Player::Role Interface::role_reader(string role){
	if(role == "-Joker")
		return Player::JOKER;
	if(role == "-Villager")
		return Player::SIMPLE_VILLAGER;
	if(role == "-Mafia")
		return Player::SIMPLE_MAFIA;
	if(role == "-Detective")
		return Player::DETECTIVE;
	if(role == "-Doctor")
		return Player::DOCTOR;
	if(role == "-RouinTan")
		return Player::ROUINTAN;
	if(role == "-GodFather")
		return Player::GODFATHER;
	if(role == "-Silencer")
		return Player::SILENCER;
	else
		throw new UnknownRole();
}

void Interface::join(istringstream& words){
	string username;

	while(words >> username)
		god.join_player_to_this_room(username);

	god.list_players();
}
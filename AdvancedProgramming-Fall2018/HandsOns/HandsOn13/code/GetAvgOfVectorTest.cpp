#include "get_avg_of_vector.hpp"
#include <stdexcept>
#include <cassert>
#include <cmath>

class GetAvgOfVectorTest
{
public:
	void run(){
		check_get_avg_of_vector();
	}

private:
	void check_get_avg_of_vector(){
		std::vector<int> numbers;

		try{
			get_avg_of_vector(numbers);
			assert(false);
		}catch(std::length_error){}

		numbers.push_back(5);
		assert( abs(get_avg_of_vector(numbers) - 5) < pow(10,-6) );

		numbers.push_back(6);
		assert( abs(get_avg_of_vector(numbers) - 5.5) < pow(10,-6) );

		numbers.push_back(6);
		assert( abs(get_avg_of_vector(numbers) - 5.6666666667) < pow(10,-6) );

		std::vector<int> numbers2;
		for(int i=1; i<=20; i++)
			numbers2.push_back(i);
		assert( abs(get_avg_of_vector(numbers2) - 10.5) < pow(10,-6) );

		std::vector<int> numbers3;
		numbers3.push_back(3);
		numbers3.push_back(3);
		numbers3.push_back(3);
		assert( abs(get_avg_of_vector(numbers3) - 3) < pow(10,-6) );
	}
	
};


int main(){
	GetAvgOfVectorTest t;
	t.run();

	return 0;
}
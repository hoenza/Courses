#include <iostream>
#include <cmath>
#include <vector>
using namespace std;


class Shape{

public:
	Shape(string _ID){
		Shape_ID = _ID;
	}

	virtual int Area()=0;
	virtual int Vol()=0;

	bool check_ID(string _ID){
		return _ID == Shape_ID;
	}
	virtual string type()=0;


protected:
	string Shape_ID;

private:
};
//********************************************
class Cylinder : public Shape{

public:

	Cylinder(string _ID, int _r, int _h)
	: Shape(_ID){
		r = _r;
		h = _h;
	}

	int Area(){
		return 2*M_PI*pow(r,2)+2*M_PI*r*h;
	}
	int Vol(){
		return h*M_PI*pow(r,2);
	}
	string type(){
		return "Cylinder";
	}

private:
	int r, h;
};
//********************************************
class Cube : public Shape{

public:
	
	Cube(string _ID, int _a, int _b, int _h)
	: Shape(_ID){
		a = _a;
		b = _b;
		h = _h;
	}

	int Area(){
		return 2*(a*b+a*h+b*h);
	}
	int Vol(){
		return h*a*b;
	}
	string type(){
		return "Cube";
	}

private:
	int a,b,h;
};
//********************************************
class Sphere : public Shape{

public:

	Sphere(string _ID, int _r)
	: Shape(_ID){
		r = _r;
	}

	int Area(){
		return 4*M_PI*pow(r,2);
	}
	int Vol(){
		return 4*M_PI*pow(r,3)/3;
	}
	string type(){
		return "Sphere";
	}

private:
	int r;
};
//********************************************
class Cone : public Shape{

public:

	Cone(string _ID,int _r, int _h)
	: Shape(_ID){
		r = _r;
		h = _h;
	}

	int Area(){
		return r*M_PI*sqrt( pow(r,2)+pow(h,2) ) + M_PI*pow(r,2);
	}
	int Vol(){
		return M_PI*pow(r,2)*h/3;
	}
	string type(){
		return "Cone";
	}

private:
	int r,h;
};
//*/*/*/*/*/*/*/*/*/**/*/*/**/*/*/*/*/*/*/*/*/**//**/*/*/*/*/*//**/*/*//*
void print_request(vector<Shape*> v, string req){
	string _ID;
	cin >> _ID;

	int i;
	for(i=0; i<v.size(); i++){
		if(v[i]->check_ID(_ID))
			break;
		else if(i== v.size()-1){
		cout << "Not found!" << endl;
		return;
		}
	}
	if(req == "area")
		cout << v[i]->type() << " area : " << v[i]->Area() << endl;

	if(req == "volume")
		cout << v[i]->type() << " volume : " << v[i]->Vol() << endl;

}
//*/*/*/**//**/*/*/*/*/*/*/*//**/*/********/*///*/*/*/*/*/*/*


int main(){

	string shape;
	vector<Shape*> v;

	while(cin >> shape){
		
		if(shape == "area" || shape == "volume")	{
		print_request(v, shape);
		continue;
	}
		string _ID;
		cin >> _ID;
		bool true_ID=true;

		for(int i=0; i<v.size(); i++){
			true_ID = !(v[i]->check_ID(_ID));
			if(!true_ID) break;
		}

		if(true_ID){
			if(shape=="cylinder"){
					int _r, _h;
					cin >> _r >> _h;
					Cylinder *sh = new Cylinder(_ID,_r,_h);
					v.push_back(sh);
				}
			else if(shape=="cube"){
					int _a, _b, _h;
					cin >> _a >> _b >> _h;
					Cube *sh = new Cube(_ID,_a,_b,_h);
					v.push_back(sh);
				}
			else if(shape=="sphere"){
					int _r;
					cin >> _r;
					Sphere *sh = new Sphere(_ID,_r);
					v.push_back(sh);
				}
			else if(shape=="cone"){
					int _r, _h;
					cin >> _r >> _h;
					Cone *sh = new Cone(_ID,_r,_h);
					v.push_back(sh);
				}
		}
		else{
			cout << "Command failed!" << endl;
		}

	}



	return 0;
}
#include <iostream>
#include <queue>
using namespace std; 
  
void interleave(queue<int> &q) 
{ 
    int size = q.size();
    queue<int> q1, q2;

    for(int i=0; i<size/2; i++) {
        q1.push(q.front());
        q.pop();
    }
    for(int i=0; i<size/2; i++) {
        q2.push(q.front());
        q.pop();
    }

    for(int i=0; i<size/2; i++) {
        q.push(q1.front());
        q1.pop();
        q.push(q2.front());
        q2.pop();
    }
}
  
int main() 
{ 
    int n;
    cin >> n;
    queue<int> q;

    for (int i = 0; i < n; i++)
    {
        int p;
        cin >> p;
        q.push(p);
    }  

    interleave(q); 

    for (int i = 0; i < n; i++) 
    { 
        cout << q.front() << ' ';
        q.pop();
    }
} 
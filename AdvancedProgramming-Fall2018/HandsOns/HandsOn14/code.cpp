#include <iostream>
using namespace std;

template<typename T>
class stack {
public:
	class Iterator {
	public:
		T next_element();
		bool has_more_elements();
	private:
		int index;
		int it_count;
		T* it_array;
		Iterator(T*,int);
		friend class stack;
	};

public:
	stack(int);
	~stack();

	void push(T);
	T pop();
	T top();
	int elem_count();

	Iterator get_iterator() { return Iterator(Array,count); }

private:
	T* Array;
	int count;
};


template<typename T>
T stack<T>::Iterator::next_element() { return it_array[index++]; }

template<typename T>
bool stack<T>::Iterator::has_more_elements() { return index != it_count; }

template<typename T>
stack<T>::Iterator::Iterator(T* array, int cnt) {
	index = 0;
	it_count = cnt;
	it_array = array;
}

template<typename T>
stack<T>::stack(int size) {
	Array = new T[size];
	count = 0;
}

template<typename T>
stack<T>::~stack() { delete[] Array; }

template<typename T>
void stack<T>::push(T new_elem) {	Array[count++] = new_elem; }

template<typename T>
T stack<T>::pop() { return Array[--count]; }

template<typename T>
T stack<T>::top() { return Array[count-1]; }

template<typename T>
int stack<T>::elem_count() { return count; }

int main() {
	stack<int> s(10);
	stack<string> a(20);
	s.push(4);
	s.push(400);
	a.push("ali");
	a.push("mamad");
	int sum = 0;
	stack<int>::Iterator it = s.get_iterator();
	while (it.has_more_elements()){
		sum += it.next_element();
	}
	cout << sum << endl;
}
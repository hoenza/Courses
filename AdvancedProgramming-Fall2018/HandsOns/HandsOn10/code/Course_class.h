#ifndef COURSE_H
#define COURSE_H

#include <iostream>
#include <vector>
using namespace std;

class Course{
public:
	Course(string,string);
	Course();

	void Add_student(string);
	bool Name_Cheker(string);
	void Print_Course_info();
	void print_course_name();
	void Print_Students_list();
	bool operator== (const Course &c1) const;
private:
	string name;
	string professor;
	vector<string> students;
};

#endif
#ifndef STUDENT_H
#define STUDENT_H

#include "Course_class.h"
#include <iostream>
#include <vector>
using namespace std;

class Course;

class Student{
public:
	Student(string,vector<Course*>);
	bool Name_Cheker(string);
	void Print_Courses_list();
private:
	string username;
	vector<Course*> courses;
};

#endif
#ifndef PROFESSOR_H
#define PROFESSOR_H

#include "Course_class.h"
#include <iostream>
using namespace std;

class Course;

class Professor{
public:
	Professor(string,Course);
	bool Username_Cheker(string _username);
	int Find_Course(vector<Course>);
private:
	string username;
	Course course;
};

#endif
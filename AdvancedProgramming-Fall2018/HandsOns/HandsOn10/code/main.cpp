#include <iostream>
#include <vector>
#include <bits/stdc++.h>
#include "Student_class.h"
#include "Professor_class.h"
using namespace std;

enum Request{
	course_add,
	student_add,
	course_info,
	student_list,
	course_list,
	wrong_request
};
Request Request_Reader(string req){
	if(req == "course")
		return course_add;
	if(req == "student")
		return student_add;
	if(req == "course_info")
		return course_info;
	if(req == "student_list")
		return student_list;
	if(req == "course_list")
		return course_list;
	else
		return wrong_request;
}

int main(){
	vector<Course> courses;
	vector<Student> students;
	vector<Professor> professors;
	string line;

	while(true){

		getline(cin,line);
		istringstream words(line);
		string request;

		words >> request;
		switch(Request_Reader(request)){
			case course_add:{
				string course_name, teacher_username;
				words >> course_name >> teacher_username;
				courses.push_back(Course(course_name,teacher_username));
				professors.push_back(Professor(teacher_username,courses[courses.size()-1]));
				break;
			}
			case student_add:{
				string student_username, course_name;
				vector<Course*> students_courses;

				words >> student_username;
				while(words >> course_name){
					for(int i=0; i<courses.size(); i++)
						if(courses[i].Name_Cheker(course_name)){
							students_courses.push_back(&courses[i]);
							break;
						}
				}
				students.push_back(Student(student_username,students_courses));
				break;
			}
			case course_info:{
				string course_name;
				words >> course_name;
				int i=0;
				for(; i<courses.size(); i++)
					if(courses[i].Name_Cheker(course_name))
						break;

				courses[i].Print_Course_info();
			break;
			}
			case student_list:{
				string professor_username;
				words >> professor_username;
				int i=0;
				for(; i<professors.size(); i++)
					if(professors[i].Username_Cheker(professor_username))
						break;
				courses[professors[i].Find_Course(courses)].Print_Students_list();
			break;
			}
			case course_list:{
				string student_username;
				words >> student_username;
				int i=0;
				for(; i<students.size(); i++)
					if(students[i].Name_Cheker(student_username))
						break;

				students[i].Print_Courses_list();
			break;
			}
		}
	}
}
#include <iostream>
#include <vector>
#include <iomanip>

using namespace std;


void print_board(vector<int> v){
	for(int i=0; i<v.size(); i++)
		for(int j=0; j<v.size(); j++){
			if(j==v.size()-1){
				if(j==v[i]) cout << '#' << endl;
				else cout << '.' << endl;
			}
			else{
				if(j==v[i]) cout << '#' << ' ';
				else cout << '.' << ' ';
			}
		}
	cout << endl;
}
#include <iostream>
#include <bits/stdc++.h>
using namespace std;

#define JOKER "-Joker"
#define VILLAGER "-Villager"
#define DETECTIVE "-Detective"
#define DOCTOR "-Doctor"
#define ROUINTAN "-RouinTan"
#define MAFIA "-Mafia"
#define GODFATHER "-GodFather"
#define SILENCER "-Silencer"

class Exeption
{
public:
	Exeption(string _rror){
		rror = _rror;
	}
	string rror;
};

class Room
{
public:
	Room(string _name){
		name = _name;
	}
	bool check_name(string _name){
		return (name == _name);
	}
	void Add_role(string role_name, int role_number){}

private:
	string name;
};

bool check_role_name(string name){
	return(
		name == JOKER ||
		name == VILLAGER ||
		name == DETECTIVE ||
		name == DOCTOR ||
		name == ROUINTAN ||
		name == MAFIA ||
		name == GODFATHER ||
		name == SILENCER
		);
}




int main(){
	vector<Room*> rooms;
	Room* selected_room;

	while(true){

		try{
			string line;
			getline(cin,line);

			istringstream commands(line);
			string command;

			commands >> command;
			if(command == "create_room"){

				string room_name, role_name;
				int role_number;
				commands >> room_name;
				
				for(int i=0; i<rooms.size(); i++)
					if(rooms[i]->check_name(room_name)) throw Exeption("Repetitive Room name : " + room_name);
				rooms.push_back(new Room(room_name));

				while(commands >> role_name){
					if(!check_role_name(role_name)){
						delete rooms[rooms.size()-1];
						rooms.pop_back();
						throw Exeption("Wrong Role name : " + role_name);
					}
					if(commands >> role_number){
						if(role_number >= 0)
							rooms[rooms.size()-1]->Add_role(role_name,role_number);
						else{
							delete rooms[rooms.size()-1];
							rooms.pop_back();
							throw Exeption("number of role cann't be less than 0!");
						}
					}
					else{
						delete rooms[rooms.size()-1];
						rooms.pop_back();
						throw Exeption("Wrong format, need integer.");
					}
				}
			}
			else if(command == "switch_room"){
				string room_name;
				commands >> room_name;
				
				int i;
				for (i = 0; i<rooms.size(); ++i)
					if(rooms[i]->check_name(room_name)) break;
				if(i == rooms.size()) throw Exeption("No such Room name : " + room_name);

				else selected_room = rooms[i];
			}
			else throw Exeption("Wrong command : " + command);
		}
		catch(Exeption e){
			cout << e.rror << endl;
		}

	}

}
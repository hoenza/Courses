#include <iostream>
using namespace std;

bool palindrome(string a){
	for(int i=0; i<a.size()/2; i++)
		if(a[i] != a[a.size()-i-1]) return false;
	return true;}

char Reverse(char a){
	switch(a){
		case 'A': return 'A'; break;
		case 'E': return '3'; break;
		case 'H': return 'H'; break;
		case 'I': return 'I'; break;
		case 'J': return 'L'; break;
		case 'L': return 'J'; break;
		case 'M': return 'M'; break;
		case 'O': return 'O'; break;
		case 'T': return 'T'; break;
		case 'S': return '2'; break;
		case 'U': return 'U'; break;
		case 'V': return 'V'; break;
		case 'W': return 'W'; break;
		case 'X': return 'X'; break;
		case 'Y': return 'Y'; break;
		case 'Z': return '5'; break;
		case '1': return '1'; break;
		case '2': return 'S'; break;
		case '3': return 'E'; break;
		case '5': return 'Z'; break;
		case '8': return '8'; break;
		default: return '\0';}	}

bool mirrored(string a){
	if(a.size()%2 !=0 && a[a.size()/2] != Reverse(a[a.size()/2])) return false;
	for(int i=0; i<a.size()/2; i++)
		if(a[i] != Reverse(a[a.size()-i-1])) return false;
	return true;}


int main(){
	string a;
		while (getline(cin,a)){
		if(palindrome(a) && mirrored(a))
			cout << a << " -- is a mirrored palindrome.";
		else if(mirrored(a))
			cout << a << " -- is a mirrored string.";
		else if(palindrome(a))
			cout << a << " -- is a regular palindrome.";
		else cout << a << " -- is not a palindrome.";
		cout << endl << endl;}
}
